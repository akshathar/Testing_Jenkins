//SELECTS WEEKLY FREQUENCY WITH COMBINATION OF TODAY,YESTERDAY,LAST 7 DAYS,LAST WEEK,LAST 30 DAYS,LAST MONTH AND LAST QUARTER

package NPS_OVERVIEW;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

/**
 * Created by omoto on 11/11/17.
 */
public class Weekly_Dateperiod extends Login{

    @Test(priority = 6)
    //Selects weekly freqency
    public void Weekly_Frequency_Selection() {
        driver.findElement(By.cssSelector("span.radio-btn-wrap:nth-child(2) > label:nth-child(2)")).click();
        System.out.println("Selected weekly radio button\n");
    }

    @Test(priority = 7)
    //Selects today date period
    public void Today_Date_Period() {
        //Clicks on dateperiod dropdown menu
        driver.findElement(By.cssSelector("label.time_duration:nth-child(2) > span:nth-child(1) > button:nth-child(1)")).click();
        System.out.println("Clicked on date period dropdown menu\n");
        //Selects today date period
        driver.findElement(By.xpath("html/body/div[1]/div/div/div/div/div/div/div[2]/div/div[1]/div[1]/div/label/span/ul/li[1]/a")).click();
        System.out.println("Selected today date period\n");
        try{
            Thread.sleep(8000);
        }catch (InterruptedException ie1) {
            ie1.printStackTrace();
        }
        System.out.println("waited for 8000ms\n");
    }

    @Test(priority = 8)
    //Selects yesterday date period
    public void Yesterday_Date_Period() {
        //Clicks on dateperiod dropdown menu
        driver.findElement(By.cssSelector("label.time_duration:nth-child(2) > span:nth-child(1) > button:nth-child(1)")).click();
        System.out.println("Clicked on date period dropdown menu\n");
        //Selects yesterday date period
        driver.findElement(By.xpath("html/body/div[1]/div/div/div/div/div/div/div[2]/div/div[1]/div[1]/div/label/span/ul/li[1]/a")).click();
        System.out.println("Selected yesterday date period\n");
        try{
            Thread.sleep(8000);
        }catch (InterruptedException ie1) {
            ie1.printStackTrace();
        }
        System.out.println("waited for 8000ms\n");
    }

    @Test(priority = 9)
    //Selects last 7 days date period
    public void Last_7days_Date_Period() {
        //Clicks on dateperiod dropdown menu
        driver.findElement(By.cssSelector("label.time_duration:nth-child(2) > span:nth-child(1) > button:nth-child(1)")).click();
        System.out.println("Clicked on date period dropdown menu\n");
        //Selects last 7days date period
        driver.findElement(By.xpath("html/body/div[1]/div/div/div/div/div/div/div[2]/div/div[1]/div[1]/div/label/span/ul/li[3]/a")).click();
        System.out.println("Selected last 7days date period\n");
        try{
            Thread.sleep(8000);
        }catch (InterruptedException ie1) {
            ie1.printStackTrace();
        }
        System.out.println("waited for 8000ms\n");
    }

    @Test(priority = 10)
    //Selects last week date period
    public void Last_Week_Date_Period() {
        //Clicks on dateperiod dropdown menu
        driver.findElement(By.cssSelector("label.time_duration:nth-child(2) > span:nth-child(1) > button:nth-child(1)")).click();
        System.out.println("Clicked on date period dropdown menu\n");
        //Selects last week date period
        driver.findElement(By.cssSelector(".open > ul:nth-child(2) > li:nth-child(4) > a:nth-child(1)")).click();
        System.out.println("Selected last week date period\n");
        try{
            Thread.sleep(8000);
        }catch (InterruptedException ie1) {
            ie1.printStackTrace();
        }
        System.out.println("waited for 8000ms\n");
    }

    @Test(priority = 11)
    //Selects last 30 days date period
    public void Last_30days_Date_Period() {
        //Clicks on dateperiod dropdown menu
        driver.findElement(By.cssSelector("label.time_duration:nth-child(2) > span:nth-child(1) > button:nth-child(1)")).click();
        System.out.println("Clicked on date period dropdown menu\n");
        //Selects last 30days date period
        driver.findElement(By.xpath("html/body/div[1]/div/div/div/div/div/div/div[2]/div/div[1]/div[1]/div/label/span/ul/li[5]/a")).click();
        System.out.println("Selected last 30days date period\n");
        try{
            Thread.sleep(8000);
        }catch (InterruptedException ie1) {
            ie1.printStackTrace();
        }
        System.out.println("waited for 8000ms\n");
    }

    @Test(priority = 12)
    //Selects last month date period
    public void Last_Month_Date_Period() {
        //Clicks on dateperiod dropdown menu
        driver.findElement(By.cssSelector("label.time_duration:nth-child(2) > span:nth-child(1) > button:nth-child(1)")).click();
        System.out.println("Clicked on date period dropdown menu\n");
        //Selects last month date period
        driver.findElement(By.xpath("html/body/div[1]/div/div/div/div/div/div/div[2]/div/div[1]/div[1]/div/label/span/ul/li[6]/a")).click();
        System.out.println("Selected last month date period\n");
        try{
            Thread.sleep(8000);
        }catch (InterruptedException ie1) {
            ie1.printStackTrace();
        }
        System.out.println("waited for 8000ms\n");
    }

    @Test(priority = 13)
    //Selects last quarter date period
    public void Last_Quarter_Date_Period() {
        //Clicks on dateperiod dropdown menu
        driver.findElement(By.cssSelector("label.time_duration:nth-child(2) > span:nth-child(1) > button:nth-child(1)")).click();
        System.out.println("Clicked on date period dropdown menu\n");
        //Selects last quarter date period
        driver.findElement(By.xpath("html/body/div[1]/div/div/div/div/div/div/div[2]/div/div[1]/div[1]/div/label/span/ul/li[7]/a")).click();
        System.out.println("Selected last quarter date period\n");
        try{
            Thread.sleep(8000);
        }catch (InterruptedException ie1) {
            ie1.printStackTrace();
        }
        System.out.println("waited for 8000ms\n");
    }

}
