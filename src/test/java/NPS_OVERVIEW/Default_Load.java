//THIS PROGRAM CHECKS WHETHER THE DEFAULT LOAD IS CORRECT OR NOT , THE DEFAULT LOAD SHOULD BE LAST 7 DAYS DATE PERIOD AND DAILY FREQUENCY

package NPS_OVERVIEW;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

/**
 * Created by omoto on 11/11/17.
 */
public class Default_Load extends Login {

    //Checks the whether the default date period is last 7 days
    @Test(priority=7)
    public void Checks_Default_Dateperiod() {
        String currentdateperiod = driver.findElement(By.xpath("html/body/div[1]/div/div/div/div/div/div/div[2]/div/div[1]/div[1]/div/label/span/button")).getText();
        System.out.println(currentdateperiod);
        String defaultdateperiod = "Last 7 Days";
        if(currentdateperiod.equals(defaultdateperiod))
        {
            System.out.println("Default load selected is last 7 days\n");
        }
        else
        {
            System.out.println("Default load is not correct\n");
        }
    }

    //checks whether the default frequency is daily
    @Test(priority=8)
    public void Checks_Default_Frequency() {
        if(driver.findElement(By.id("daily")).isSelected()) {
            System.out.println("Default frequency selected i.e daily frequency\n");
        }
        else {
            System.out.println("Default frequency is not correct\n");
        }
    }

    //alternate method for default frquency selection
	/*String currentfreq = driver.findElement(By.id("daily")).getAttribute("checked");
	if (currentfreq.equalsIgnoreCase("false")){
		System.out.println("daily frequency Checkbox selected");
	}*/
}
