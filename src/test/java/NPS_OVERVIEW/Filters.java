//Selects filters for srl

package NPS_OVERVIEW;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

/**
 * Created by omoto on 11/11/17.
 */
public class Filters extends Lastquarter{

    //Selects south from zone filter
    @Test(priority=7)
    public void Zone_Filter() {
        //Clicks on zone filter
        driver.findElement(By.cssSelector("ul.col-sm-3:nth-child(6) > label:nth-child(2) > span:nth-child(1) > button:nth-child(1)")).click();
        System.out.println("Clicked on zone fileter\n");
        //Selects south form the drop down menu]
        driver.findElement(By.cssSelector("#mCSB_18_container > li:nth-child(6) > a:nth-child(1)")).click();
        System.out.println("north selected\n");
        //Waits for 8 seconds
        try{
            Thread.sleep(8000);
        }catch (InterruptedException ie1) {
            ie1.printStackTrace();
        }
        System.out.println("waited for 8000ms\n");
    }

    //Selects collection center visit email from campaign
    @Test(priority=8)
    public void Campaign_Filter(){
        //Clicks on campaign filter
        driver.findElement(By.xpath("html/body/div[1]/div/div/div/div/div/div/div[2]/div/div[1]/div[2]/div/ul[1]/label/span/button")).click();
        System.out.print("Clicked on campaign filter");
        //Selects collection center visit email
        driver.findElement(By.xpath(".//*[@id='mCSB_26_container']/li[1]")).click();
        System.out.print("Collection center visit email\n");

        try{
            Thread.sleep(8000);
        }catch (InterruptedException ie1) {
            ie1.printStackTrace();


        }
        System.out.println("waited for 8000ms\n");
    }

    //Selects network lab from labtype filter
    @Test(priority=9)
    public void Labtype_Filter(){
        //Clicks on Labtype filter
        driver.findElement(By.xpath("html/body/div[1]/div/div/div/div/div/div/div[2]/div/div[1]/div[2]/div/ul[2]/label/span/button")).click();
        System.out.print("Clicked on Labtype filter");
        //Selects network lab
        driver.findElement(By.xpath(".//*[@id='mCSB_27_container']/li[4]/a")).click();
        System.out.print("Collection network lab\n");

        try{
            Thread.sleep(8000);
        }catch (InterruptedException ie1) {
            ie1.printStackTrace();
        }
        System.out.println("waited for 8000ms\n");
    }

}
