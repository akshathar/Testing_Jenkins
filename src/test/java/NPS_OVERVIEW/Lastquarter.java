//Selects last quarter date period

package NPS_OVERVIEW;

import NPS_OVERVIEW.Login;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

/**
 * Created by omoto on 11/11/17.
 */
public class Lastquarter extends Login {

    @Test(priority = 6)
    //Selects last quarter date period
    public void Last_Quarter_Date_Period() {
        //Clicks on dateperiod dropdown menu
        driver.findElement(By.cssSelector("label.time_duration:nth-child(2) > span:nth-child(1) > button:nth-child(1)")).click();
        System.out.println("Clicked on date period dropdown menu\n");
        //Selects last quarter date period
        driver.findElement(By.xpath("html/body/div[1]/div/div/div/div/div/div/div[2]/div/div[1]/div[1]/div/label/span/ul/li[7]/a")).click();
        System.out.println("Selected last quarter date period\n");
        try{
            Thread.sleep(8000);
        }catch (InterruptedException ie1) {
            ie1.printStackTrace();
        }
        System.out.println("waited for 8000ms\n");
    }
}
