//THIS PROGRAM LOGS IN TO THE APPLICATION WITH THE SPECIFIED EMAILID AND PASSWORD,SELECTS THE DASHBOARD,NAVIGATES TO SECOND PAGE OF DASHBOARD,SELECTS HAMBARGER MENU AND SELECTS NPS OVERVIEW

package NPS_OVERVIEW;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

/**
 * Created by omoto on 11/11/17.
 */
public class Login {

    public WebDriver driver ;

    //Opens the Firefox browser
    @Test(priority=1)
    public void Opens_Browser() {
        System.setProperty("webdriver.firefox.marionette","C:\\geckodriver.exe");
        driver=new FirefoxDriver();
        System.out.println("Firefox browser opened");
    }

    //Navigates to the url and logs in with specified email and password
    @Test(priority=2)
    public void Navigates_To_Url_and_Logs_in() {
        driver.get("http://testvm2.omoto.io/omoto/#/login");
        System.out.println("navigated to url\n");
        driver.findElement(By.name("email")).sendKeys("srl@omoto.io");
        driver.findElement(By.name("password")).sendKeys("password");
        System.out.println("Successfully logged in provided name and password\n");
    }

    //Selects dashboard and navigates to second page of dashboard
    @Test(priority=3)
    public void Selects_Dashboard() {
        driver.findElement(By.cssSelector("div.login_container:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > form:nth-child(3) > div:nth-child(1) > div:nth-child(3)")).click();
        System.out.println("Dash board successfully opened\n");
        try{
            Thread.sleep(8000);
        }catch (InterruptedException ie1) {
            ie1.printStackTrace();
        }
        System.out.println("waited for 8000ms\n");

        //Selects dash board second page
        driver.findElement(By.cssSelector(".glyphicon-chevron-right")).click();
        System.out.println("Navigated to second page of dashboard\n");
    }

    //Selects hambarger menu
    @Test(priority=4)
    public void Selects_Hambarger_Menu() {
        driver.findElement(By.cssSelector("#simple-btn-keyboard-nav")).click();
        System.out.println("Selected the hambarger menu\n");
    }

    //Selects nps overview
    @Test(priority=5)
    public void Selects_Nps_Overview() {
        driver.findElement(By.xpath(".//*[@id='navbar-collapse-main']/ul/li[4]/div/ul/div/li[1]/a")).click();
        System.out.println("Selected nps overview\n");
        try{
            Thread.sleep(8000);
        }catch (InterruptedException ie1) {
            ie1.printStackTrace();
        }
        System.out.println("waited for 8000ms\n");
    }

}
